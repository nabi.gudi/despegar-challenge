## The project

This project is the result of Despegar's challenge. The challenge consists in develop the cluster corresponding to the result of Hotels and the cluster of results of Flights. 

## The skeleton
The tooling and criteria used for this project was:
* Create React App
* Responsive Web Design
* Mobile First

## How to run the app?
`npm start` runs the app in development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. 

## How it works?
### Flight Result Cluster
Open [http://localhost:3000/flights](http://localhost:3000/flights) to view it in the browser. 
It receive the info from a JSON file. 
### Hotel Result Cluster
Open [http://localhost:3000/hotels](http://localhost:3000/hotels) to view it in the browser. 
It receive the info from a JSON file (provided by Despegar). 