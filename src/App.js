import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import './App.scss';
import Flights from './flights/Flights';
import Hotels from './hotels/Hotels';

function App() {
  return (  
    <div className="Home">
      <React.Fragment>
        <div className="main-container">
          <BrowserRouter>
            <Switch>
              <Route exact path="/" ></Route>
              <Route exact path="/flights" component={Flights} ></Route>
              <Route exact path="/hotels" component={Hotels} ></Route>
            </Switch>
          </BrowserRouter>
        </div>
        
        
      </React.Fragment>
    </div>
  );
}

export default App;