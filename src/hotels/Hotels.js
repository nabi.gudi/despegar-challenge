import React from 'react';
import './hotels.scss';
import HotelImage from './components/HotelImage/HotelImage';
import HotelInfo from './components/HotelInfo/HotelInfo';
import HotelPayment from './components/HotelPayment/HotelPayment';
import HotelPromo from './components/HotelPromo/HotelPromo';
import hotels from './hotels.json';

function Hotels() {  
  return ( 
    <div>
      {
        hotels.results.map((hotel) => (
          <div className="Hotel">
            <HotelImage hotel={hotel}></HotelImage>
            <HotelInfo hotel={hotel}></HotelInfo>
            <div className="container">
                <HotelPayment hotel={hotel}></HotelPayment>
                <HotelPromo hotel={hotel}></HotelPromo>
            </div>
          </div>
        ))}
    </div>
  )
}

export default Hotels;