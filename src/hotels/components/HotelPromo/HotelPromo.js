import React, {Component} from 'react';
import './HotelPromo.scss';

export default class HotelPromo extends Component {
  render() {
    return (
      <div className="Hotel_promo">
        <label className="Hotel_promo-text">Paga el alojamiento hasta en 12 cuotas sin interés <a href="http://localhost:3000/hotels">Ver bancos y tarjetas</a></label>
      </div>
    )
  }
}
