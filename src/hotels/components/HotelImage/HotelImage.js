import React, {Component} from 'react';
import './HotelImage.scss';
import hotel from '../../img/hotel.jpg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons';


export default class HotelImage extends Component {
  render() {
    return (
      <div className="Hotel_image">
        <img className="Hotel_image-img" src={hotel} alt="foto-hotel"></img>
        <div className="Hotel_image_extras">
          <FontAwesomeIcon className="Hotel_image_extras-icon" icon={faCoffee} /> 
          <label>Desayuno incluido</label>
        </div>
        
      </div>
    )
  }
}
