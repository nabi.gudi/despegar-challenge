import React, {Component} from 'react';
import './HotelInfo.scss';
import tripadvisor from '../../img/tripadvisor.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faDotCircle } from '@fortawesome/free-solid-svg-icons';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

import { faWifi } from '@fortawesome/free-solid-svg-icons';
import { faParking } from '@fortawesome/free-solid-svg-icons';
import { faDumbbell } from '@fortawesome/free-solid-svg-icons';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { faTshirt } from '@fortawesome/free-solid-svg-icons';

export default class HotelInfo extends Component {
  render() {
    let hotel = this.props.hotel;
    let stars = Array(Number(hotel.stars))
      .fill(<FontAwesomeIcon icon={faStar} /> );
    return (
      <div className="Hotel_info">
          <label className="Hotel_info-name">{hotel.name}</label>
          <div className="Hotel_info_address">
            <label className="Hotel_info_address-city">Miami, Miami Springs</label>
            <label className="Hotel_info_address-distance">A 5,75km del centro.</label>
          </div>
          <a className="Hotel_info-mapView" href="#">Ver mapa</a>
          <div className="Hotel_info_data">
            <label className="Hotel_info_data-score">{hotel.rating}</label>
            {
              Number(hotel.stars) >= 1 ? (
                <div className="Hotel_info_data-stars">              
                  {stars}
                </div>   
              ) : null
            }        
            <label className="Hotel_info_data-separator">|</label>
            <div className="Hotel_info_data_amenities">
              {
                hotel.amenities.map((element, index) => (
                  console.log(element),
                  (hotel.amenities[index]==="wifi") ?
                  <FontAwesomeIcon className="Hotel_info_data_amenities-items" icon={faWifi} /> 
                  : 
                  ( hotel.amenities[index]==="gym") ?
                    <FontAwesomeIcon className="Hotel_info_data_amenities-items" icon={faDumbbell} /> 
                    :
                    (hotel.amenities[index]==="parking") ? 
                      <FontAwesomeIcon className="Hotel_info_data_amenities-items" icon={faParking} />
                      :
                      (hotel.amenities[index]==="coffee") ? 
                        <FontAwesomeIcon className="Hotel_info_data_amenities-items" icon={faCoffee} />
                        :
                        (hotel.amenities[index]==="laundry") ? 
                          <FontAwesomeIcon className="Hotel_info_data_amenities-items" icon={faTshirt} /> 
                          : null
                ))
              }
            </div>
          </div>
          <div className="Hotel_info-scoreTripAdvisor">
              <img className="Hotel_info-scoreTripAdvisor-logo" src={tripadvisor} alt="Trip-Advisor-Logo"></img>
              <div className="Hotel_info-scoreTripAdvisor-score">             
                <FontAwesomeIcon icon={faDotCircle} />             
                <FontAwesomeIcon icon={faDotCircle} />             
                <FontAwesomeIcon icon={faDotCircle} />             
                <FontAwesomeIcon icon={faCircle} />                         
                <FontAwesomeIcon icon={faCircle} />
              </div>
              
          </div>
          <div className="Hotel_info_extras" >
            {
               hotel.features.map((element, index) => (
                <label className="Hotel_info_extras-items">{hotel.features[index]}</label>
              ))
            }
          </div>
      </div>
    )
  }
}
