import React, {Component} from 'react';
import './HotelPayment.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

export default class HotelPayment extends Component {
  render() {
    let hotel = this.props.hotel;
    return (
      <div className="Hotel_payment">
          
          <label className="Hotel_payment-final">Precio final por 7 noches para 2 personas</label>
          <div className="Hotel_payment_price">
            <a href="http://localhost:3000/hotels" className="Hotel_payment_price-info">
              <FontAwesomeIcon icon={faInfoCircle} />
            </a>
            <label className="Hotel_payment_price-price">${hotel.price}</label>
            <button className="Hotel_payment_price-btn">Ver detalle</button>
          </div>         
      </div>
    )
  }
}
