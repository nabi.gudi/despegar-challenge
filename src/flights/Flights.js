import React, { Component } from 'react';
import FlightInfo from './components/FlightInfo/FlightInfo';
import FlightPoints from "./components/FlightPoints/FlightPoints";
import FlightSelection from "./components/FlightSelection/FlightSelection";
import './flights.scss';
import flights from './flights.json';

export default class Flights extends Component {
  render() {
    return (
      <div className="flights">
        <FlightInfo
          results={flights.results}
        ></FlightInfo>
        <FlightPoints
          points={flights.points}
        ></FlightPoints>
        <FlightSelection
          selection={flights.selection}
        ></FlightSelection>
      </div>
    );
  };
}