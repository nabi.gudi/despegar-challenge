import React, { Component } from 'react';
import './FlightSelection.scss';

export default class FlightSelection extends Component {
  render() {
    let selection = this.props.selection;
    return (
      <div className="flights_selection">
        <span className="flights_selection_comment">{selection.comment}</span>
        <span className="flights_selection_price_amount">${selection.amount}</span>
        <span className="flights_selection_price_title">{selection.title}</span>
        <div className="flights_selection_buy">
          <button className="flights_selection_buy-btn">{selection.buy}</button>
        </div>
        <div className="flights_selection_promo">
          <span className="flights_selection_promo_financing">{selection.financing}</span>
          <a className="flights_selection_promo_banks" href="http://localhost:3000/flights">{selection.banks}</a>
        </div>
      </div>
    )
  }
}