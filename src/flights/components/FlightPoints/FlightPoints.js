import React, { Component } from 'react';
import './FlightPoints.scss';

export default class FlightPoints extends Component {
  render() {
    let points = this.props.points;
    return (
      <div className="flights_points">
        <span>{points.title}</span>
      </div>
    )
  }
}