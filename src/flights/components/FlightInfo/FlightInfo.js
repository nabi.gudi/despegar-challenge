import React, { Component } from 'react';
import FlightData from './FlightData/FlightData';
import './FlightInfo.scss'

export default class FlightInfo extends Component {
  render() {
    let results = this.props.results;
    let departures = [];
    let arrivals = [];
    results.forEach(element => {
      if (element.trackTitle === "Ida") {
        departures.push(element);
      } else {
        arrivals.push(element);
      }
    });
    return (
      <div className="flights_info">
      {
        departures.length > 0 ? (
          <FlightData
            data={departures}
          ></FlightData>
        ) : []
      }
      {
        arrivals.length > 0 ? (
          <FlightData
            data={arrivals}
          ></FlightData>
        ) : []
      }
      </div>
    )
  }
}