import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlane } from '@fortawesome/free-solid-svg-icons';
import './FlightHeader.scss';

export default class FlightHeader extends Component {
  render() {
    let header = this.props.header;
    return (
      <div className="flights_info_data_header">
        <div className="flights_info_data_header-track">
          <div className="flights_info_data_header-track_title">
            <FontAwesomeIcon className="flights_info_data_header-track_title-icon" icon={faPlane} />
            <label className="flights_info_data_header-track_title-text">{header.trackTitle}</label>
          </div>
          <label className="flights_info_data_header-track_date">{header.trackDate}</label>
        </div>
        <label className="flights_info_data_header-from_code">{header.fromCode}</label>
        <label className="flights_info_data_header-from_title">{header.fromTitle}</label>
        <label className="flights_info_data_header-to_code">{header.toCode}</label>
        <label className="flights_info_data_header-to_title">{header.toTitle}</label>
        <label className="flights_info_data_header-luggage_title">Equipaje</label>
      </div>
    )
  }
}