import React, { Component } from 'react';
import FlightHeader from './FlightHeader/FlightHeader';
import FlightBody from './FlightBody/FlightBody';
import "./FlightData.scss";

export default class FlightData extends Component {
  render() {
    let data = this.props.data;
    let header = {};
    let pivot = data[0];
    data.forEach(element => {
      if (
        pivot.fromTitle === element.fromTitle &&
        pivot.toTitle === element.toTitle
      ) {
        header = {};
        header = {
          "trackTitle": element.trackTitle,
          "trackDate": element.trackDate,
          "fromTitle": element.fromTitle,
          "fromCode": element.fromCode,
          "toTitle": element.toTitle,
          "toCode": element.toCode,
        }
      }
    });
    return (
      <div
        className="flights_info_data"
      >
        <FlightHeader
          header={header}
        ></FlightHeader>
        {
          data.map((element, index) => (
            <FlightBody
              key={index}
              fromTitle={element.fromTitle}
              fromCode={element.fromCode}
              fromTime={element.fromTime}
              toTitle={element.toTitle}
              toCode={element.toCode}
              toTime={element.toTime}
              toPlus={element.toPlus}
              stopovers={element.stopovers}
              durationTime={element.durationTime}
            ></FlightBody>
          ))
        }
      </div>
    )
  }
}