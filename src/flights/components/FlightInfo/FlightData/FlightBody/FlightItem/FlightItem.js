import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faSuitcase } from '@fortawesome/free-solid-svg-icons';
import { faSuitcaseRolling } from '@fortawesome/free-solid-svg-icons';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
import airline from '../../../../../img/airline.png';
import './FlightItem.scss';

export default class FlightItem extends Component {
  render() {
    let item = this.props.item;
    let stopovers = Array(Number(item.stopovers))
      .fill(<div className="flights_info_data_body_item-stopovers_points_wrapper_point"></div>);
    return (
      <div className="flights_info_data_body_item">
        <img className="flights_info_data_body_item-airline" src={airline} alt="Airline Logo"></img>
        <a href="http://localhost:3000/flights" className="flights_info_data_body_item-info" >
          <FontAwesomeIcon className="flights_info_data_body_item-info-icon_mobile" icon={faInfoCircle} />
          <FontAwesomeIcon className="flights_info_data_body_item-info-icon_desktop"icon={faCaretDown} />
        </a>
        <input className="flights_info_data_body_item-radio" type="radio"/>
        <span className="flights_info_data_body_item-from_code">{item.fromCode}</span>
        <span className="flights_info_data_body_item-from_time">{item.fromTime}</span>
        <div className="flights_info_data_body_item-stopovers">
          <p className="flights_info_data_body_item-stopovers_title">
            {
              Number(item.stopovers) === 0 ? (
                `directo`
              ) : Number(item.stopovers) === 1 ? (
                `${item.stopovers} escala`
              ) : (
                `${item.stopovers} escalas`
              )
            }
          </p>   
          {
            Number(item.stopovers) >= 1 ? (
              <div className="flights_info_data_body_item-stopovers_points">
                <hr className="flights_info_data_body_item-stopovers_points_line"/>
                <div className="flights_info_data_body_item-stopovers_points_wrapper">
                  {stopovers}
                </div>
              </div>
            ) : null
          }
        </div>
        <span className="flights_info_data_body_item-to_code">{item.toCode}</span>
        <span className="flights_info_data_body_item-to_time">{item.toTime}</span>
        <span className="flights_info_data_body_item-to_plus">{item.toPlus}</span>
        <div className="flights_info_data_body_item-luggage_icons">
          <FontAwesomeIcon className="flights_info_data_body_item-luggage_icons-icon" icon={faSuitcase} />
          <FontAwesomeIcon className="flights_info_data_body_item-luggage_icons-icon" icon={faSuitcaseRolling} />
        </div>
        <div className="flights_info_data_body_item-duration">
          <span className="flights_info_data_body_item-duration-title">Duración:</span>        
          <span className="flights_info_data_body_item-duration-time">{item.durationTime}</span>
        </div>
      </div>
    );
  };
}