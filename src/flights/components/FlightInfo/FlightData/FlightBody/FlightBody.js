import React, { Component } from 'react';
import FlightItem from "./FlightItem/FlightItem";
import './FlightBody.scss'

export default class FlightBody extends Component {
  render() {
    let item = this.props;
    return (
      <div className="flights_info_data_body">
        <FlightItem
          item={item}
        ></FlightItem>
      </div>
    );
  };
}